#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <string>
#include <sstream>

class Buro{
	private:
		int id;
		string buro = "Istanbul avukatlar b�rosu";
		
	public:
		int getId(){
			setId();
			return id;
		}
		string getBuroName() {
			return buro;
		}
		int setId() {
			srand(time(0));
			int new_id = rand() % (99999 - 10000 -1) + 10000; // 5 basamakl� bir id olu�turur.
			id = new_id;
		}
		bool setBuroName(string newName){
			buro = newName;
		}
		void changeBuroName(){
			string newname;
			cout << "Yeni buro ad�: ";
			cin.ignore();
			getline(cin, newname);
	
			setBuroName(newname);
			cout << "B�ro ad� de�i�tirildi\n";
			cout << "-----------------------\n";
		}

		string convert_id_to_string(int id) {
			stringstream ss;
			string str_id;
			ss << id;
			ss >> str_id;
			return str_id;
		}

};

