#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <ctime>
#include <time.h>
#include <stdlib.h>
#include <sstream>


using namespace std;


class User{
	private:
		int can_pass = 0;
		int id;
		string kullanici;
	public:
		void setPass(int num) {
			can_pass = num;
		}
		bool setLogin(string kadi, string ksifre){
			ifstream avukatlar;
	        avukatlar.open("veriler/user/avukatlar.txt", ios::in);
	        string okunan;
	        if (kadi == "" || ksifre == ""){
	        	avukatlar.close();
	        	return false;
			}
	        while (!avukatlar.eof()) {
	            getline(avukatlar, okunan);
	            if (okunan.find(kadi) != string::npos && okunan.find(ksifre) != string::npos) {
	            	avukatlar.close();
	            	kullanici = kadi;
	            	
	            	setPass(1);
	            	
	            	ofstream user_dosya;
					cout << "Giri� ba�ar�l�\n";
					user_dosya.open("veriler/user/log.txt", ios::app);
	
					user_dosya << kullanici << " giris yapti: " << getNowDate();
					user_dosya.close();
	                return true;
	                break;
	            }
	        }
	        avukatlar.close();
	        return false;
		}
		bool getPass(){
			if(can_pass == 1)
				return true;
			return false;
		}
		string getNowDate() {
			time_t now = time(0);
			char* now_date = ctime(&now);
			return now_date;
		}
		User(string kadi, string ksifre){
			setLogin(kadi, ksifre);
		}
		~User(){
			if(getPass()){
				ofstream user_dosya;
				user_dosya.open("veriler/user/log.txt", ios::app);
				user_dosya << kullanici << " cikis yapti: " << getNowDate();
				setPass(0);
			}		
		}
		int getId() {
			srand(time(0));
			id = rand() % (99999 - 10000 -1) + 10000; // 5 basamakl� bir id olu�turur.
			return id;
		}
		
		string getStrId(int id) {
			stringstream ss;
			string str_id;
			ss << id;
			ss >> str_id;
			return str_id;
		}
};
