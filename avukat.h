#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include "buro.h"  // id olu�turmak i�in yazd���m fonksiyonu �a��r�yorum.



class Avukat: public Buro{
	using Buro::Buro;
	public:
		bool avukat_ekle() {
			ofstream dosya;
			dosya.open("veriler/user/avukatlar.txt", ios::app);
			
			string kadi;
			string ksifre;
			string name;
			string uzmanlik;
			int id = getId();
			
			cout << "Kullan�c� ad�: ";
			cin >> kadi;
			cout << "Kullan�c� �ifre: ";
			cin >> ksifre;
			cout << "Ad: ";
			cin.ignore();
			getline(cin, name);
			cout << "Uzmanl�k alan�: ";
			cin.ignore();
			getline(cin, uzmanlik);
		
			
			dosya << id << " " << kadi << " " << ksifre << "\t" << name << " " << uzmanlik << " "<< getBuroName() << "\n";
			cout << "Olu�turulan avukat�n ID: " << id << "\n";
			dosya.close();
			return true;

		}
		void avukat_listele() {
			ifstream dosya_oku;
			string okunan;
			dosya_oku.open("veriler/user/avukatlar.txt", ios::in);
			while (!dosya_oku.eof()) {
				getline(dosya_oku, okunan);
				cout << okunan << "\n";
			}
			dosya_oku.close();
		}
		
		void avukat_ara(){
			string okunan, aranan;
			int uzunluk1, uzunluk2;
			ifstream verioku;
			verioku.open("veriler/user/avukatlar.txt", ios::in);
			cout << "Aramak istediginiz veriyi giriniz: ";
			cin >> aranan;
		
			while (!verioku.eof())
			{
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				for (int i = 0; i < (uzunluk1-uzunluk2); i++)
				{
					if (aranan.compare(okunan.substr(i, uzunluk2)) == 0)
					{
						cout << "\n\nAranan avukat buluundu: " << endl;
						cout << okunan << endl << endl;
						break;
					}
				}
			}
			verioku.close();
		}
		
		void avukat_guncelle(){
			int id = getId();
			string kadi;
			string ksifre;
			string name;
			string uzmanlik;
			

			
			
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true,bul=false;
			
			ifstream verioku;
			verioku.open("veriler/user/avukatlar.txt", ios::in);
			
			cout << "G�ncellemek istediginiz avukat�n id'sini giriniz giriniz: ";
			cin >> aranan;
			
			int satir=1;
			
			string tt="";
			while (!verioku.eof())
			{		
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				
				if(aranan.compare(okunan.substr(0,5)) == 0)
				{
					bul=true;
					yazma=false;
					string str_id = convert_id_to_string(id);
					
					cout << "Aranan avukat" << endl;
					cout << okunan << endl;
					
					cout << "Kullan�c� ad�: ";
					cin >> kadi;
					cout << "Kullan�c� �ifre: ";
					cin >> ksifre;
					cout << "Ad: ";
					cin.ignore();
					getline(cin, name);
					cout << "Uzmanl�k alan�: ";
					cin.ignore();
					getline(cin, uzmanlik);
					
					yedek=yedek + str_id + " " + kadi + " " + ksifre + "\t" + name + " " + uzmanlik + " " + getBuroName() +"\n";
				}
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
				satir++;
			}
			
			verioku.close();
			
			if(bul)
			{
				ofstream veriyaz;
				veriyaz.open("veriler/user/avukatlar.txt");
				veriyaz<<yedek;
				veriyaz.close();
				cout<<"G�ncelleme ba�ar�l�. Yeni ID: " << id << "\n";
			}
			else{
				cout<<"Girdi�iniz ID listede yok.\n";
			}
		}
		
		void avukat_sil(){
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true;
			
			ifstream verioku;
			verioku.open("veriler/user/avukatlar.txt", ios::in);
			
			cout << "Silmek istediginiz avukat�n id'sini giriniz: ";
			cin >> aranan;
			
			
			while (!verioku.eof())
			{
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				if (aranan.compare(okunan.substr(0, 5)) == 0)
				{
					yazma=false;				
				}
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
			}
			cout<<yedek;
			
			verioku.close();
			
			ofstream veriyaz;
			veriyaz.open("veriler/user/avukatlar.txt");
			veriyaz<<yedek;
			veriyaz.close();
			cout << "Ba�ar�yla silindi \n";
		}
};
