#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include "user.h"



using namespace std;


class Dava: public User{
	using User::User;
	public:
		void veri_ekle() {
			ofstream dosya;
			dosya.open("veriler/dava.txt", ios::app);
			int id = getId();
			char zaman[100];
			char tur[100];
			char sanik[255];
			char magdur[255];
			

			cout << "Dava ne zaman?: ";
			cin.getline(zaman, 100);
			cin.ignore();
			cout << "Dava ne ile alakal�?: ";
			cin.getline(tur, 100);
			cin.ignore();
			cout << "San�k: (Davac�): ";
			cin.getline(sanik, 255);
			cin.ignore();
			cout << "Ma�dur (�ddia makam�): ";
			cin.getline(magdur, 255);
			cin.ignore();
		
			
			dosya << id << "\t" << zaman << "\t" << tur << "\t\t" << sanik << "\t\t" << magdur << "\n";
			dosya.close();
			cout << "Olu�turuluan dava ID: " << id << "\n";

		}
		void veri_listele() {
			ifstream dosya_oku;
			string okunan;
			dosya_oku.open("veriler/dava.txt", ios::in);
			while (!dosya_oku.eof()) {
				getline(dosya_oku, okunan);
				cout << okunan << "\n";
			}
			dosya_oku.close();
		}
		
		void veri_ara(){
			string okunan, aranan;
			int uzunluk1, uzunluk2;
			ifstream verioku;
			verioku.open("veriler/dava.txt", ios::in);
			cout << "Aramak istediginiz veriyi giriniz: ";
			cin >> aranan;
		
			while (!verioku.eof())
			{
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				for (int i = 0; i < (uzunluk1-uzunluk2); i++)
				{
					if (aranan.compare(okunan.substr(i, uzunluk2)) == 0)
					{
						cout << "Aranan kayit" << endl;
						cout << okunan << endl;
						break;
					}
				}
			}
			verioku.close();
		}
		
		void veri_guncelle(){
			int id = getId();
			char zaman[100];
			char tur[100];
			char sanik[255];
			char magdur[255];
			
			
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true,bul=false;
			
			ifstream verioku;
			verioku.open("veriler/dava.txt", ios::in);
			
			cout << "Aramak istediginiz veriyi giriniz: ";
			cin >> aranan;
			
			int satir=1;
			
			string tt="";
			while (!verioku.eof())
			{		
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				
				if(aranan.compare(okunan.substr(0,5)) == 0)
				{
					bul=true;
					yazma=false;
					
					cout << "Aranan kayit" << endl;
					cout << okunan << endl;
					
					string str_id = getStrId(id);
			
					cout << "Dava ne zaman?: ";
					cin.getline(zaman, 100);
					cout << "Dava ne ile alakal�?: ";
					cin.getline(tur, 100);
					cout << "San�k: ";
					cin.getline(sanik, 255);
					cout << "Ma�dur (�ddia makam�): ";
					cin.getline(magdur, 255);
					
					yedek=yedek + str_id + "\t" + zaman + "\t" + tur + "\t\t" + sanik + "\t\t" + magdur + "\n";
				}
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
				satir++;
			}
			
			verioku.close();
			
			if(bul)
			{
				ofstream veriyaz;
				veriyaz.open("veriler/dava.txt");
				veriyaz<<yedek;
				veriyaz.close();
				cout<<"G�ncelleme ba�ar�l�. Yeni ID: " << id << "\n";
			}
			else{
				cout<<"Girdi�iniz dava ID listede yok.\n";
			}
		}
		
		void veri_sil(){
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true;
			
			ifstream verioku;
			verioku.open("veriler/dava.txt", ios::in);
			
			cout << "Aramak istediginiz davan�n ID'sini giriniz: ";
			cin >> aranan;
			
			
			while (!verioku.eof())
			{
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				if (aranan.compare(okunan.substr(0, 5)) == 0)
				{
					cout << "Ba�ar�yla silindi\n";
					yazma=false;				
				}
				/*for (int i = 0; i < (uzunluk1-uzunluk2); i++)
				{
				
					
				}*/
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
			}
			cout<<yedek;
			
			verioku.close();
			
			ofstream veriyaz;
			veriyaz.open("veriler/dava.txt");
			veriyaz<<yedek;
			veriyaz.close();
		}
		
};


