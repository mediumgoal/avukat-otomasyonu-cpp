#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <locale.h>


#include <ctime>
#include <time.h>
#include <stdlib.h>
#include <sstream>
// #include "avukat.h"


using namespace std;

class Buro{
	private:
		int id;
		string buro = "Istanbul avukatlar b�rosu";
		
	public:
		int getId(){
			setId();
			return id;
		}
		string getBuroName() {
			return buro;
		}
		int setId() {
			srand(time(0));
			int new_id;
			generate_id_loop:
				new_id = rand() % (99999 - 10000 -1) + 10000; // 5 basamakl� bir id olu�turur.
			ifstream dosyaOku;
			dosyaOku.open("veriler/user/avukatlar.txt", ios::in);
			string okunan;
			while(!dosyaOku.eof()){
				getline(dosyaOku, okunan);
				if(okunan.find(id) != string::npos){
					goto generate_id_loop;
				}
			}
			id = new_id;
		}
		bool setBuroName(string newName){
			buro = newName;
		}
		void changeBuroName(){
			string newname;
			cout << "Yeni buro ad�: ";
			cin.ignore();
			getline(cin, newname);
	
			setBuroName(newname);
			cout << "B�ro ad� de�i�tirildi\n";
			cout << "-----------------------\n";
		}

		string convert_id_to_string(int id) {
			stringstream ss;
			string str_id;
			ss << id;
			ss >> str_id;
			return str_id;
		}

};


class Avukat: public Buro{
	using Buro::Buro;
	private:
		string buro_name = getBuroName();
	public:
		Avukat(string new_buro_name){
			this->buro_name = new_buro_name;
			setBuroName(new_buro_name);
		}
		~Avukat(){
			this->buro_name = getBuroName();
		}
		bool avukat_ekle() {
			ofstream dosya;
			dosya.open("veriler/user/avukatlar.txt", ios::app);
			
			string kadi;
			string ksifre;
			string name;
			string uzmanlik;
			int id = getId();
			
			cout << "Kullan�c� ad�: ";
			cin >> kadi;
			cout << "Kullan�c� �ifre: ";
			cin >> ksifre;
			cout << "Ad: ";
			cin.ignore();
			getline(cin, name);
			cout << "Uzmanl�k alan�: ";
			cin.ignore();
			getline(cin, uzmanlik);
		
			
			dosya << id << " " << kadi << " " << ksifre << "\t" << name << " " << uzmanlik << " "<< getBuroName() << "\n";
			cout << "Olu�turulan avukat�n ID: " << id << "\n";
			dosya.close();
			return true;

		}
		void avukat_listele() {
			ifstream dosya_oku;
			string okunan;
			dosya_oku.open("veriler/user/avukatlar.txt", ios::in);
			while (!dosya_oku.eof()) {
				getline(dosya_oku, okunan);
				cout << okunan << "\n";
			}
			dosya_oku.close();
		}
		
		void avukat_ara(){
			string okunan, aranan;
			int uzunluk1, uzunluk2;
			ifstream verioku;
			verioku.open("veriler/user/avukatlar.txt", ios::in);
			cout << "Aramak istediginiz veriyi giriniz: ";
			cin >> aranan;
		
			while (!verioku.eof())
			{
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				for (int i = 0; i < (uzunluk1-uzunluk2); i++)
				{
					if (aranan.compare(okunan.substr(i, uzunluk2)) == 0)
					{
						cout << "\n\nAranan avukat buluundu: " << endl;
						cout << okunan << endl << endl;
						break;
					}
				}
			}
			verioku.close();
		}
		
		void avukat_guncelle(){
			int id = getId();
			string kadi;
			string ksifre;
			string name;
			string uzmanlik;
			
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true,bul=false;
			
			ifstream verioku;
			verioku.open("veriler/user/avukatlar.txt", ios::in);
			
			cout << "G�ncellemek istediginiz avukat�n id'sini giriniz giriniz: ";
			cin >> aranan;
			
			int satir=1;
			
			string tt="";
			while (!verioku.eof())
			{		
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				
				if(aranan.compare(okunan.substr(0,5)) == 0)
				{
					bul=true;
					yazma=false;
					string str_id = convert_id_to_string(id);
					
					cout << "Aranan avukat" << endl;
					cout << okunan << endl;
					
					cout << "Kullan�c� ad�: ";
					cin >> kadi;
					cout << "Kullan�c� �ifre: ";
					cin >> ksifre;
					cout << "Ad: ";
					cin.ignore();
					getline(cin, name);
					cout << "Uzmanl�k alan�: ";
					cin.ignore();
					getline(cin, uzmanlik);
					
					yedek=yedek + str_id + " " + kadi + " " + ksifre + "\t" + name + " " + uzmanlik + " " + getBuroName() +"\n";
				}
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
				satir++;
			}
			
			verioku.close();
			
			if(bul)
			{
				ofstream veriyaz;
				veriyaz.open("veriler/user/avukatlar.txt");
				veriyaz<<yedek;
				veriyaz.close();
				cout<<"G�ncelleme ba�ar�l�. Yeni ID: " << id << "\n";
			}
			else{
				cout<<"Girdi�iniz ID listede yok.\n";
			}
		}
		
		void avukat_sil(){
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true;
			
			ifstream verioku;
			verioku.open("veriler/user/avukatlar.txt", ios::in);
			
			cout << "Silmek istediginiz avukat�n id'sini giriniz: ";
			cin >> aranan;
			
			
			while (!verioku.eof())
			{
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				if (aranan.compare(okunan.substr(0, 5)) == 0)
				{
					yazma=false;				
				}
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
			}
			cout<<yedek;
			
			verioku.close();
			
			ofstream veriyaz;
			veriyaz.open("veriler/user/avukatlar.txt");
			veriyaz<<yedek;
			veriyaz.close();
			cout << "Ba�ar�yla silindi \n";
		}
};



class User{
	private:
		int can_pass = 0;
		string kullanici;
	public:
		void setPass(int num) {
			can_pass = num;
		}
		bool setLogin(string kadi, string ksifre){
			ifstream avukatlar;
	        avukatlar.open("veriler/user/avukatlar.txt", ios::in);
	        string okunan;
	        if (kadi == "" || ksifre == ""){
	        	avukatlar.close();
	        	return false;
			}
	        while (!avukatlar.eof()) {
	            getline(avukatlar, okunan);
	            if (okunan.find(kadi) != string::npos && okunan.find(ksifre) != string::npos) {
	            	avukatlar.close();
	            	this->kullanici = kadi;
	            	
	            	setPass(1);
	            	
	            	ofstream user_dosya;
					cout << "Giri� ba�ar�l�\n";
					user_dosya.open("veriler/user/log.txt", ios::app);
	
					user_dosya << kullanici << " giris yapti: " << getNowDate();
					user_dosya.close();
	                return true;
	                break;
	            }
	        }
	        avukatlar.close();
	        cout << "Giri� ba�ar�s�z. tekrar deneyin\n";
	        return false;
		}
		bool getPass(){
			if(this->can_pass == 1)
				return true;
			return false;
		}
		string getNowDate() {
			time_t now = time(0);
			char* now_date = ctime(&now);
			return now_date;
		}
		User(string kadi, string ksifre){
			this->kullanici = kadi;
			setLogin(kadi, ksifre);
		}
		~User(){
			if(getPass()){
				ofstream user_dosya;
				user_dosya.open("veriler/user/log.txt", ios::app);
				user_dosya << kullanici << " cikis yapti: " << getNowDate();
				setPass(0);
			}					
		}

		
};



class Dava: public User{
	using User::User;
	private:
		int id;
		int getId() {
			srand(time(0));
			generate_id_loop:
				id = rand() % (99999 - 10000 -1) + 10000;
			ifstream dosyaOku;
			dosyaOku.open("veriler/dava.txt", ios::in);
			string okunan;
			while(!dosyaOku.eof()){
				getline(dosyaOku, okunan);
				if(okunan.find(id) != string::npos){
					goto generate_id_loop;
				}
			}
			
			return id;
		}
		
		string getStrId(int id) {
			stringstream ss;
			string str_id;
			ss << id;
			ss >> str_id;
			return str_id;
		}
	public:
		void veri_ekle() {
			ofstream dosya;
			dosya.open("veriler/dava.txt", ios::app);
			int id = getId();
			char zaman[100];
			char tur[100];
			char sanik[255];
			char magdur[255];
			

			cout << "Dava ne zaman?: ";
			//cin.ignore();
			cin.getline(zaman, 100);
			
			cout << "Dava ne ile alakal�?: ";
			cin.getline(tur, 100);
			//cin.ignore();
			cout << "San�k: (Daval�): ";
			cin.getline(sanik, 255);
			//cin.ignore();
			cout << "Ma�dur (Davac�): ";
			cin.getline(magdur, 255);
			//cin.ignore();
		
			
			dosya << id << "\t" << zaman << "\t" << tur << "\t\t" << sanik << "\t\t" << magdur << "\n";
			dosya.close();
			cout << "Olu�turuluan dava ID: " << id << "\n";

		}
		void veri_listele() {
			ifstream dosya_oku;
			string okunan;
			dosya_oku.open("veriler/dava.txt", ios::in);
			while (!dosya_oku.eof()) {
				getline(dosya_oku, okunan);
				cout << okunan << "\n";
			}
			dosya_oku.close();
		}
		
		void veri_ara(){
			string okunan, aranan;
			int uzunluk1, uzunluk2;
			ifstream verioku;
			verioku.open("veriler/dava.txt", ios::in);
			cout << "Aramak istediginiz veriyi giriniz: ";
			cin >> aranan;
		
			while (!verioku.eof())
			{
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				for (int i = 0; i < (uzunluk1-uzunluk2); i++)
				{
					if (aranan.compare(okunan.substr(i, uzunluk2)) == 0)
					{
						cout << "Aranan kayit" << endl;
						cout << okunan << endl;
						break;
					}
				}
			}
			verioku.close();
		}
		
		void veri_guncelle(){
			int id = getId();
			char zaman[100];
			char tur[100];
			char sanik[255];
			char magdur[255];
			
			
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true,bul=false;
			
			ifstream verioku;
			verioku.open("veriler/dava.txt", ios::in);
			
			cout << "Aramak istediginiz veriyi giriniz: ";
			cin >> aranan;
			
			int satir=1;
			
			string tt="";
			while (!verioku.eof())
			{		
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				
				if(aranan.compare(okunan.substr(0,5)) == 0)
				{
					bul=true;
					yazma=false;
					
					cout << "Aranan kayit" << endl;
					cout << okunan << endl;
					
					string str_id = getStrId(id);
			
					cout << "Dava ne zaman?: ";
					cin.ignore();
					cin.getline(zaman, 100);
					cout << "Dava ne ile alakal�?: ";
					cin.getline(tur, 100);
					cout << "San�k (Daval�): ";
					cin.getline(sanik, 255);
					cout << "Ma�dur (Davac�): ";
					cin.getline(magdur, 255);
					
					yedek=yedek + str_id + "\t" + zaman + "\t" + tur + "\t\t" + sanik + "\t\t" + magdur + "\n";
				}
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
				satir++;
			}
			
			verioku.close();
			
			if(bul)
			{
				ofstream veriyaz;
				veriyaz.open("veriler/dava.txt");
				veriyaz<<yedek;
				veriyaz.close();
				cout<<"G�ncelleme ba�ar�l�. Yeni ID: " << id << "\n";
			}
			else{
				cout<<"Girdi�iniz dava ID listede yok.\n";
			}
		}
		
		void veri_sil(){
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true;
			
			ifstream verioku;
			verioku.open("veriler/dava.txt", ios::in);
			
			cout << "Aramak istediginiz davan�n ID'sini giriniz: ";
			cin >> aranan;
			
			
			while (!verioku.eof())
			{
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				if (aranan.compare(okunan.substr(0, 5)) == 0)
				{
					cout << "Ba�ar�yla silindi\n";
					yazma=false;				
				}
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
			}
		
			
			verioku.close();
			
			ofstream veriyaz;
			veriyaz.open("veriler/dava.txt");
			veriyaz<<yedek;
			veriyaz.close();
		}
		friend bool is_logined(Dava Dava);
		friend int generate_id(Dava Dava);
		friend string get_str_id(Dava Dava, int id);
};

bool is_logined(Dava Dava){
	if(Dava.getPass() == 1){
		return true;
	}
	return false;
}

int generate_id(Dava Dava){
	return Dava.getId();
}

string get_str_id(Dava Dava, int id){
	return Dava.getStrId(id);
}

class Ajanda{
	private:
		int getId() {
			int id;
			srand(time(0));
			generate_id_loop:
				id = rand() % (99999 - 10000 -1) + 10000;
			ifstream dosyaOku;
			dosyaOku.open("veriler/ajanda.txt", ios::in);
			string okunan;
			while(!dosyaOku.eof()){
				getline(dosyaOku, okunan);
				if(okunan.find(id) != string::npos){
					goto generate_id_loop;
				}
			}
			return id;
		}
		
		string getStrId(int id) {
			stringstream ss;
			string str_id;
			ss << id;
			ss >> str_id;
			return str_id;
		}
	public:

		void veri_ekle() {
			ofstream dosya;
			dosya.open("veriler/ajanda.txt", ios::app);
			int id = getId();
			char zaman[200];
			char islem[300];

			
			cout << "Yap�lacak i�: ";
			cin.ignore();
			cin.getline(islem, 300);
			
			cout << "Ne zaman yap�lacak: ";
			cin.ignore();
			cin.getline(zaman, 200);
			
			cout << "�SLEM: " << islem << " " << zaman << " \n"; 

			dosya << id << " " << islem << " " << zaman << "\n";
			dosya.close();
			cout << "Olu�turuluan dava ID: " << id << "\n";

		}
		void veri_listele() {
			ifstream dosya_oku;
			string okunan;
			dosya_oku.open("veriler/ajanda.txt", ios::in);
			while (!dosya_oku.eof()) {
				getline(dosya_oku, okunan);
				cout << okunan << "\n";
			}
			dosya_oku.close();
		}
		
		void veri_ara(){
			string okunan, aranan;
			int uzunluk1, uzunluk2;
			ifstream verioku;
			verioku.open("veriler/ajanda.txt", ios::in);
			cout << "Aramak istediginiz veriyi giriniz: ";
			cin >> aranan;
		
			while (!verioku.eof())
			{
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				for (int i = 0; i < (uzunluk1-uzunluk2); i++)
				{
					if (aranan.compare(okunan.substr(i, uzunluk2)) == 0)
					{
						cout << "Aranan kay�t" << endl;
						cout << okunan << endl;
						break;
					}
				}
			}
			verioku.close();
		}
		
		void veri_guncelle(){
			int id = getId();
			char zaman[200];
			char islem[300];
			
			
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true,bul=false;
			
			ifstream verioku;
			verioku.open("veriler/ajanda.txt", ios::in);
			
			cout << "Aramak istediginiz veriyi giriniz: ";
			cin >> aranan;
			
			int satir=1;
			
			string tt="";
			while (!verioku.eof())
			{		
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				
				if(aranan.compare(okunan.substr(0,5)) == 0)
				{
					bul=true;
					yazma=false;
					
					cout << "Aranan kayit" << endl;
					cout << okunan << endl;
					
					string str_id = getStrId(id);
			
						cout << "Yap�lacak i�: ";
					cin.ignore();
					cin.getline(islem, 300);
					
					cout << "Ne zaman yap�lacak: ";
					cin.ignore();
					cin.getline(zaman, 200);
					
					yedek=yedek + str_id + " " + islem + " " + zaman + "\n";
				}
				if (yazma && uzunluk1 > 1)
				{
					yedek = yedek + okunan + "\n";			
				}
				satir++;
			}
			
			verioku.close();
			
			if(bul)
			{
				ofstream veriyaz;
				veriyaz.open("veriler/ajanda.txt");
				veriyaz<<yedek;
				veriyaz.close();
				cout<<"G�ncelleme ba�ar�l�. Yeni ID: " << id << "\n";
			}
			else{
				cout<<"Girdi�iniz dava ID listede yok.\n";
			}
		}
		
		void veri_sil(){
			string okunan, aranan, yedek="";
			int uzunluk1, uzunluk2;
			bool yazma=true;
			
			ifstream verioku;
			verioku.open("veriler/ajanda.txt", ios::in);
			
			cout << "Aramak istediginiz davan�n ID'sini giriniz: ";
			cin >> aranan;
			
			
			while (!verioku.eof())
			{
				yazma=true;
				getline(verioku, okunan);
				uzunluk1 = okunan.length();
				uzunluk2 = aranan.length();
				if (aranan.compare(okunan.substr(0, 5)) == 0){
					cout << "Ba�ar�yla silindi\n";
					yazma=false;				
				}
				if (yazma && uzunluk1 > 1){
					yedek = yedek + okunan + "\n";			
				}
			}
			cout<<yedek;
			
			verioku.close();
			
			ofstream veriyaz;
			veriyaz.open("veriler/ajanda.txt");
			veriyaz<<yedek;
			veriyaz.close();
		}
		
};



int main(int argc, char** argv) {
	
	setlocale(LC_ALL, "Turkish");
	char devam = 'e';

	Dava Dava("", "");
	Avukat Avukat("Istanbul avukatlar burosu");
	Ajanda Ajanda;

	
	do {
		int menu_sira1;
		int menu_sira2;
		string kad;
		string ksifre;
		
		cout << "-----------------------\n";
		cout << "Ho� geldiniz S.N Avukat, l�tfen se�im yap�n�z \n";
		cout << "1: Avukat men�s�\n";
		cout << "2: Dava men�s�\n";
		cout << "3: Ajanda men�s�\n";
		cout << "4: ��k��\n";
		cout << "-----------------------\n";
		cout << "Se�iniz: ";
		cin >> menu_sira1;
		
		switch(menu_sira1){
			case 1:

				while(menu_sira1 == 1){
					cout << "Avukat men�s�ne ho� geldiniz, bu men�den avukatlar� d�zenleyebilirsiniz\n";
					cout << "1: Avukat ekle\n";
					cout << "2: Avukat listele\n";
					cout << "3: Avukat ara\n";
					cout << "4: Avukat g�ncelle\n";
					cout << "5: Avukat sil\n";
					cout << "6: B�ro ad�n� g�ster\n";
					cout << "7: B�ro ad�n� de�i�tir\n";
					cout << "8: Geri\n";
					cin >> menu_sira2;
					cout << "----------------------\n";
					switch(menu_sira2){
						case 1:
							Avukat.avukat_ekle();
							break;
						case 2:
							Avukat.avukat_listele();
							break;
						case 3:
							Avukat.avukat_ara();
							break;
						case 4:
							Avukat.avukat_guncelle();
							break;
						case 5:
							Avukat.avukat_sil();
							break;
						case 6:
							cout << Avukat.getBuroName() << "\n";
							break;
						case 7:
							Avukat.changeBuroName();
							break;
						case 8:
							menu_sira1 = 0;
							break;
					}
					cout << "----------------------\n";
				}
				break;
			case 2:

				while(!Dava.getPass()){
					cout << "Kullanici adiniz: ";
					cin >> kad;
					cout << "�ifre giriniz: ";
					cin >> ksifre;
					Dava.setLogin(kad, ksifre);
				}
				while(menu_sira1==2){
					cout << "-----------------------\n";
					cout << "------Dava Men�s�------\n";
					cout << "-----------------------\n";
					cout << "1: Dava ekle\n";
					cout << "2: Dava listele\n";
					cout << "3: Dava ara\n";
					cout << "4: Dava g�ncelle\n";
					cout << "5: Dava sil\n";
					cout << "6: Geri\n";
					cout << "-----------------------\n";
					cout << "Se�iniz: ";
					cin >> menu_sira2;
					cin.ignore();
					switch(menu_sira2) {
						case 1:
							Dava.veri_ekle();
							break;
						case 2:
							Dava.veri_listele();
							break;
						case 3:
							Dava.veri_ara();
							break;
						case 4:
							Dava.veri_guncelle();
							break;
						case 5:
							Dava.veri_sil();
							break;
						case 6:
							menu_sira1=0;
							break;
					}
				}
				
				break;
			case 3:
				while(!is_logined(Dava)){
					cout << "Kullanici adiniz: ";
					cin >> kad;
					cout << "�ifre giriniz: ";
					cin >> ksifre;
					Dava.setLogin(kad, ksifre);
					
				}
				while(menu_sira1==3){
					cout << "-----------------------\n";
					cout << "------Ajanda Men�s�------\n";
					cout << "-----------------------\n";
					cout << "1: Ajandaya yeni bir kay�t ekle\n";
					cout << "2: Ajandam� listele\n";
					cout << "3: Ajanda da kay�t ara\n";
					cout << "4: Ajanda da kay�t g�ncelle\n";
					cout << "5: Ajanda da kay�t sil\n";
					cout << "6: Geri\n";
					cout << "-----------------------\n";
					cout << "Se�iniz: ";
					cin >> menu_sira2;
					switch(menu_sira2) {
						case 1:
							Ajanda.veri_ekle();
							break;
						case 2:
							Ajanda.veri_listele();
							break;
						case 3:
							Ajanda.veri_ara();
							break;
						case 4:
							Ajanda.veri_guncelle();
							break;
						case 5:
							Ajanda.veri_sil();
							break;
						case 6:
							menu_sira1=0;
							break;
					}
				}
				break;
			case 4:
				devam = 'h';
				break;
			default:
				cout << "Eksik ya da hatal� bir yere gitmeye �al��t�n�z\n";
				break;
		}
	
		
	}while(devam == 'e' || devam == 'E');

	return 0;
}
